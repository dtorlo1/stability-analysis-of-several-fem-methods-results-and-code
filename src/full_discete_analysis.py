#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 20 17:28:48 2021

@author: simichel
"""

""" ------- Dispersion analysis of an EDO 2D problem with the FEM ---------
    d_t U + a.U = 0
    ~> d_t U + a.d_x U = 0
    U(X,t=0) = U_0(X)  

U : density
vx and vy are simple speep in respectly x and y directions
here, vx = vy = a
#Solution of this this 2D transport equation :
#    U((x,y),t) = U(X,t) = U_0(X)*e^{-at}
# CFL : a*dt < 1
 --------------------------------------------------------------------- """

import numpy as np
from numpy.linalg import matrix_power, eig
from math import pi, pow, atan2, log, sqrt

from time_scheme import time_scheme_RK, time_scheme_SSPRK, \
time_scheme_DeC
from spacial_discretization import *

""" ----------------------- Parameters definition ---------------------- """
DIM = 1
# =============================================================================



def run_dispersion(el, ts, to, bf, dx = 0.1, \
                   A = 1., stab = 'any', CFL = 1., TAU = 0.):
    a = 1.         
    """ --------------------- Choose the time scheme --------------------- """
    if (ts == 'RK'):
        time_scheme = time_scheme_RK(to)
    elif (ts == 'SSPRK'):
        time_scheme = time_scheme_SSPRK(to)
    elif (ts == 'DeC'):
        time_scheme = time_scheme_DeC(to)
    else:       
        print('Choose an other time scheme, this one doesn t exist')
        return 0       
    nn = 40
    """ ------------------------ Scheme parameters ----------------------- """
    # 
    # k = 2pi / lambda                      lambda - wave length
    # k = 2pi / (Nc * dx)                   Nc - number of cells per wave
    # k_min = 2pi / (Nc_max * dx) -> 0      with Nc_max -> infinite
    # k_max = 2pi / (Nc_min * dx)     
    # with Nc_min which verify the Nyquist criterium : 
    # minimum of 3 nodes per wave
    # P1 - Nc_min = 2 = 2 / 1
    # P2 - Nc_min = 1 = 2 / 2
    # P3 - Nc_min = 2 / 3
    Nc_min = 2/(el)
    if (bf == 'cubature' and el == 3):
        alp = 0.5-0.1*sqrt(5)
        Nc_min = (1.-alp)
    kmax = 2*pi / (dx*Nc_min)
    K = np.linspace(0.1, kmax ,nn) 
    Theta = K * dx     
    #
    ck = [[] for l in range(el)]
    dump  = [[] for l in range(el)]
    for ik in range(nn):
        k = K[ik]
        theta = k*dx
        dt = CFL*dx      
        p = el
        G = np.eye((el))
        dispersion = choose_class_of_Dispersion(p, bf, stab, 'phase')  
        # fill matrices to solve the system :
        # [-i(w+D) M_f + SCM ] U = 0
        # <=> solve the eigenvalues sytem
        # [lambda I - M_f^{-1} SCM ] U = 0
        # M_f and SCM depend of degree of elements, stabilization method
        dispersion.fill_system_matrices(theta, dx = dx)
        tau = TAU
        if (dispersion.stab_method == 'CIP'):
            # CIP
            # M^{-1} Kx + tau(M^{-1} GJ ) 
            # Penalty = tau * dx*dx * a
            Penalty = tau * dx*dx * a
            Mp = dispersion.M_f
            SCM = a*dispersion.Kx_f + Penalty*dispersion.GJ_f 
        elif (dispersion.stab_method == 'LPS'):
            # LPS
            # M^{-1} Kx + tau(M^{-1} Kxx - M^{-1} Kxt * M^{-1} Kx)
            # Penalty = tau * dx * a
            Penalty = tau * dx * a
            Mp = dispersion.M_f 
            Q_f = np.linalg.solve(dispersion.M_f,dispersion.Kx_f)
            SCM = a*dispersion.Kx_f + Penalty*(dispersion.Kxx_f - np.dot(dispersion.Kxt_f,Q_f))
        elif (dispersion.stab_method == 'dissipation'):
            # ADD DISSIPATIVE TERM
            # M^{-1} Kx + tau(M^{-1} Kxx)
            # Penalty = tau * dx * a
            Penalty = tau * dx * a
            Mp = dispersion.M_f 
            SCM = a*dispersion.Kx_f + Penalty*(dispersion.Kxx_f )
        elif (dispersion.stab_method == 'SUPG'):
            # SUPG
            # (M + tau*Kxt)^{-1} a*Kx + tau * (M + tau*Kxt)^{-1} a*Kxx
            # Penalty = tau * dx / a
            Penalty = tau*dx/a
            Mp = dispersion.M_f + Penalty*a*dispersion.Kxt_f
            SCM = a*dispersion.Kx_f + Penalty*a*a*dispersion.Kxx_f 
        else:
            # ANY
            # M^{-1} Kx 
            Mp = dispersion.M_f
            SCM = a*dispersion.Kx_f 
        # Fourier 
        if (ts == 'DeC'):
            ML = dispersion.M_L
            A = np.linalg.solve(ML,SCM) 
            B = np.linalg.solve(ML,Mp) 
            # U^{n+1} = G * U^{n} 
            G = time_scheme.fourier_matrix1d(A, B, dt)
        elif (ts == 'RK' or 'SSPRK'):
            A = np.linalg.solve(Mp,SCM)               
            # U^{n+1} = [ I + sum_{alp} (-a dt)^{alp} Gamma_{alp} A^{alp} ] U^{n}
            # U^{n+1} = G * U^{n}
            G = np.eye((el))
            for alp in range(time_scheme.step):
                G = G + (pow((-1.*dt),(alp+1))) * \
                    time_scheme.Dispersion_G[alp] * matrix_power(A,alp+1)
        eigvals, eigvecs = eig(G)  #la.
        for l in range(el):
            wex = a*k                                               # exact value
            wt = -atan2(eigvals.imag[l],eigvals.real[l])/dt         # approximated value
            ck[l].append(dispersion.error(wt,wex,k))                # error of dispersion
            if (eigvals.real[l] == 0. and eigvals.imag[l] == 0):
                print('caution, wrong value forthe damping')
                dump[l].append(0.)
            else:
                eEt = (eigvals.real[l]**2+eigvals.imag[l]**2)       # module of the eigenvalue
                dump[l].append(log(eEt)/(2.*dt))                    # Damping
    # find the good eigenvalue
    (goodck, gooddamp) = dispersion.find_good_eigen(Theta, ck, dump, a = a)
    return (Theta, ck, dump, goodck, gooddamp, K)


# =============================================================================
# Dispersion analysis
#        for Linear Advection system
# =============================================================================
        
def fourier_analysis(basis_function, time_scheme, stabilization = 'any', \
                      cfl_tab = [0.1, 0.1, 0.1], \
                      Penalty_coef_tab = [0.01, 0.01, 0.01]):
    el_tab = [1,2,3]
    # wave length
    dx = 1.
    a = 1.
    # ---------------------- MAIN LOOP --------------------- #
    THETA = []                  # theta = k*dx, from 0 to pi
    PRINCIPAL_CK = []           # ck = w for the phase - Principal mode
    ALL_CK = []
    PRINCIPAL_DAMP = []         # Damping - Principal mode
    ALL_DAMP = []
    to = []                     # time order
    CFL = []                    # cfl
    for element in el_tab:
        # Parameters
        cfl = cfl_tab[element-1]
        tau = Penalty_coef_tab[element-1]
        #
        time_order = element +1
        CFL.append(cfl)
        to.append(time_order)
        (th, allCk, allDamp, principalCk, principalDamp, K) = run_dispersion(element, \
            time_scheme, time_order, basis_function, \
            dx = dx, A = a, stab = stabilization, CFL = cfl, TAU = tau) 
        # rescale
        th = th / element
        for j in range(len(th)):
            principalCk[j] = principalCk[j] / element
            principalDamp[j] = principalDamp[j] / element
            for i in range(element):
                allCk[i][j] = allCk[i][j] / element
                allDamp[i][j] = allDamp[i][j] / element
        PRINCIPAL_CK.append(principalCk)
        ALL_CK.append(allCk)
        PRINCIPAL_DAMP.append(principalDamp)
        ALL_DAMP.append(allDamp)
        THETA.append(th)
    return (el_tab, dx, THETA, PRINCIPAL_CK, PRINCIPAL_DAMP, ALL_CK, ALL_DAMP)
    # END OF THE ELEMENTS LOOP

""" ------------------------ Scheme parameters ------------------------ """ 
class Scheme_param():
    def __init__(self):
        self.basis_function = 'cubature'
        self.time_scheme = 'RK'
        self.stabilization = 'No stab'
        self.cfl = 0.1
        self.penalty_coef = 0.01

