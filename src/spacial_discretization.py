#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 20 17:27:13 2021

@author: simichel
"""


import numpy as np
from quadrature_formula import Quadrature_droite

import cmath as cma
from math import cos, sqrt

# =============================================================================
# Global parameters
# =============================================================================
ic = cma.sqrt(-1.)

# Approximation - extrapolation order 3 
def approx2(X,Y,c):
    Vand = np.array([[1, X[0]], \
                     [1, X[1]]])
    y = np.array([Y[0],Y[1]])
    a =  np.linalg.solve(Vand,y)
    w = a[0] + a[1]*c 
    return w

# Approximation - extrapolation order 3 
def approx3(X,Y,c):
    Vand = np.array([[1, X[0], X[0]*X[0]], \
                     [1, X[1], X[1]*X[1]], \
                     [1, X[2], X[2]*X[2]]])
    y = np.array([Y[0],Y[1],Y[2]])
    a =  np.linalg.solve(Vand,y)
    w = a[0] + a[1]*c + a[2]*c*c
    return w

# Approximation - extrapolation order 3 
def approx4(X,Y,c):
    Vand = np.array([[1, X[0], X[0]*X[0], X[0]*X[0]*X[0]], \
                     [1, X[1], X[1]*X[1], X[1]*X[1]*X[1]], \
                     [1, X[2], X[2]*X[2], X[2]*X[2]*X[2]], \
                     [1, X[3], X[3]*X[3], X[3]*X[3]*X[3]]])
    y = np.array([Y[0],Y[1],Y[2],Y[3]])    
    a =  np.linalg.solve(Vand,y)
    w = a[0] + a[1]*c + a[2]*c*c + a[3]*c*c*c
    return w

# Chosse the class of Dispersion
# depends of basis functions and degree of elements
def choose_class_of_Dispersion(el, bf, stab, Er):
    if (bf == 'cubature'):
        if (el == 1):
            return Dispersion_cubature_P1(el, bf, stab, Er)
        elif (el == 2):
            return Dispersion_cubature_P2(el, bf, stab, Er)
        elif (el == 3):
            return Dispersion_cubature_P3(el, bf, stab, Er)    
    elif (bf == 'lagrange'):
        if (el == 1):
            return Dispersion_lagrange_P1(el, bf, stab, Er)
        elif (el == 2):
            return Dispersion_lagrange_P2(el, bf, stab, Er)
        elif (el == 3):
            return Dispersion_lagrange_P3(el, bf, stab, Er)    
        elif (el == 4):
            return Dispersion_lagrange_P4(el, bf, stab, Er)  
    elif (bf == 'bernstein' or bf == 'bezier'):
        if (el == 1):
            return Dispersion_bezier_P1(el, bf, stab, Er)
        elif (el == 2):
            return Dispersion_bezier_P2(el, bf, stab, Er)
        elif (el == 3):
            return Dispersion_bezier_P3(el, bf, stab, Er)           
    else:
        return Dispersion_lagrange_P1(el, bf, stab, Er)


# =============================================================================
# Main class with main function
# =============================================================================
class Dispersion_1D:
    def __init__(self, el, bf, stab, Er):
        self.element_P = el
        self.basis_function = bf
        self.stab_method = stab
        if (self.basis_function == 'cubature'):
            self.quad = Quadrature_droite(self.element_P, dof = True, bf = 'cubature')
        else:
            self.quad = Quadrature_droite(3)
        self.quad_DOF = Quadrature_droite(self.element_P, dof = True, bf = self.basis_function)
        # label for error axis
        self.Err = Er
        self.Err_msg = 'Phase : \u03C9'

    # Choose the error in the main file 
    def error(self, w, wex, k = 1., Damping = 0.):
        return w
        
    # Choose the good eigenvelue for the graph
    def find_good_eigen(self, Theta, ck, dump, a = 1.):
        goodck = []
        gooddamp = []
        # len(Theta) = len(ck[0])
        for nth in range(len(Theta)):
            idgood = 0
            if (nth < 4):
                # for 3 first points, 
                # we choose the closest of the exact solution
                ckex = a * Theta[nth]
                for l in range(1, self.element_P):
                    if (abs(ck[l][nth]-ckex) < abs(ck[idgood][nth]-ckex)):
                        idgood = l
            else:
                # After, we choose the closest of the approximation
                # of the solution obtaine by a backward extrapolation 
                # using 3 last points
                X = np.array([Theta[nth-1], Theta[nth-2], Theta[nth-3], Theta[nth-4]])
                Yck = np.array([goodck[nth-1], goodck[nth-2], goodck[nth-3], goodck[nth-4]])
                Ydamp = np.array([gooddamp[nth-1], gooddamp[nth-2], gooddamp[nth-3], gooddamp[nth-4]])
                ckappr = approx3(X,Yck,Theta[nth])
                if (sum(abs(Ydamp[:])) < 1e-2):
                    dampappr = 0.
                else:
                    dampappr = approx3(X,Ydamp,Theta[nth])
                for l in range(1, self.element_P):
                    if ( (abs(ck[l][nth]-ckappr) < abs(ck[idgood][nth]-ckappr) + 1e-2) \
                        and (abs(dump[l][nth]-dampappr) < abs(ck[idgood][nth]-ckappr) + 0.5)):
                        idgood = l  
            goodck.append(ck[idgood][nth])
            gooddamp.append(dump[idgood][nth]) 
        return (goodck, gooddamp)
    
    # Integration by quadrature formula
    # order 7 except for cubature elements
    # cubature : quadrature formulas match with DOF
    def elementary_int(self,i,h,j,g):
        nn = len(self.quad.qp)
        p = 0.
        # quadrature sum
        for k in range(nn):
            x = self.quad.qp[k][1]
            phi_i = h(i,x)
            phi_j = g(j,x)
            p += self.quad.w[k]*phi_i*phi_j
        return p
    
    # Fill matrix with elementary integral
    # M_{ij} = int( h g )
    def fill_matrix(self,h,g):
        nn = self.element_P+1
        M = np.zeros((nn,nn))
        for i in range(nn):
            for j in range(nn):
                M[i][j] = self.elementary_int(i,h,j,g)  
        return M

    # Make the problem an eigenvalue problem 
    # ref : Spencer Sherwin 
    # Dispersion analysis of the continuous  and discontinuous Galerkine formulation
    def matrix_fourier(self,M,theta):
        em = cma.exp(-ic*theta)
        ep = cma.exp(ic*theta)  
        n1 = self.element_P
        N = np.zeros((n1,n1), dtype=complex)
        for i in range(n1):
            for j in range(n1):
                N[i][j] = M[i][j]
        for i in range(n1):
            N[0][i] += M[n1][i]*em           
            N[i][0] += M[i][n1]*ep
        N[0][0] += M[n1][n1]
        return N

    # Fill mass matrix with quadrature formula
    # M_{ij} = int( phi_i phi_j)
    def fill_M(self, theta, DX = 1., lump = False):
        if (lump == False):
            M = self.fill_matrix(self.f,self.f)
        else:
            M = np.diag(self.quad_DOF.w)
        Mf = self.matrix_fourier(M, theta)       
        return Mf     
    # Fill matrix such as 
    # Kx_{ij} int_{K} Phi_{i} dx(Phi_{j}) 
    def fill_Kx(self, theta, DX = 1.):
        M = self.fill_matrix(self.f,self.df)
        Kx = self.matrix_fourier(M, theta) * 1./DX        
        return Kx
    # Fill matrix such as 
    # Kxt_{ij} int_{K} dx(Phi_{i}) Phi_{j}
    def fill_Kxt(self, theta, DX = 1.):
        M = self.fill_matrix(self.df,self.f)
        Kxt = self.matrix_fourier(M, theta) * 1./DX
        return Kxt            
    # Fill matrix such as 
    # Kxx_{ij} int_{K} dx(Phi_{i}) dx(Phi_{j}) 
    def fill_Kxx(self, theta, DX = 1.):
        M = self.fill_matrix(self.df,self.df)
        Kxx = self.matrix_fourier(M, theta) * (1./DX**2)
        return Kxx
    #
    # Fill all necessary matrices for the system
    #  
    def fill_system_matrices(self, theta, dx = 1., a = 1.):
        self.Kx_f = dx * self.fill_Kx(theta, DX = dx)
        self.M_f = dx * self.fill_M(theta, DX = dx)
        self.M_L = dx * self.fill_M(theta, DX = dx, lump = True)
        if (self.stab_method == 'CIP'):
            # CIP
            self.GJ_f = self.fill_Gr_jump(theta) * (1/dx**2)
        elif (self.stab_method == 'LPS'):
            # LPS
            self.Kxx_f = dx * self.fill_Kxx(theta, DX = dx)
            self.Kxt_f = dx * self.fill_Kxt(theta, DX = dx)
        elif (self.stab_method == 'SUPG'):
            # SUPG
            self.Kxx_f = dx * self.fill_Kxx(theta, DX = dx)
            self.Kxt_f = dx * self.fill_Kxt(theta, DX = dx) 
        elif (self.stab_method == 'dissipation'):
            # DISSIPATIVE TERM
            self.Kxx_f = dx * self.fill_Kxx(theta, DX = dx)
            
    def __del__(self):
        del self.quad



# =============================================================================
# Basic Elements
# =============================================================================
class Dispersion_lagrange(Dispersion_1D):
    pass
    
class Dispersion_lagrange_P1(Dispersion_lagrange):
    # basis functions
    def f(self,i,x):
        if (i == 0):
            return (1.-x)
        elif (i == 1):
            return x 
    def df(self,i,x):
        if (i == 0):
            return -1.
        elif (i == 1):
            return 1.
    # Fill Gradient Jump at interface
    # Continuous Interior Penalty
    def fill_Gr_jump(self, theta):
        GJ = np.array([[(2.*cos(2.*theta)+6.-8.*cos(theta))]])
        return GJ
    
class Dispersion_lagrange_P2(Dispersion_lagrange):
    # basis functions
    def f(self,i,x):
        if (i == 0):
            return (1.-x)*(1.-2.*x)
        elif (i == 1):
            return 4.*x*(1.-x)
        elif (i == 2):
            return x*(2.*x-1.)     
    def df(self,i,x):
        if (i == 0):
            return 4.*x-3.
        elif (i == 1):
            return -8.*x+4.
        elif (i == 2):
            return 4.*x-1.

    # Fill Gradient Jump at interface
    # Continuous Interior Penalty
    def fill_Gr_jump(self, theta):
        em = cma.exp(-ic*theta)
        ep = cma.exp(ic*theta)         
        emm = cma.exp(-ic*2.*theta)
        epp = cma.exp(ic*2.*theta)         
        GJ = np.array([[24.*cos(theta)+38.+2.*cos(2.*theta), -28.-8.*cos(theta)-4.*emm-24.*em], \
                [-28.-24.*ep-8.*cos(theta)-4.*epp,          32.+32*cos(theta)]])
        return GJ

class Dispersion_lagrange_P3(Dispersion_lagrange):   
    # basis functions
    def f(self,i,x):
        if (i == 0):
            return 0.5*(1.-x)*(3.*x-1.)*(3.*x-2.)
        elif (i == 1):
            return -9./2.*x*(1.-x)*(3.*x-2.)
        elif (i == 2):
            return 9./2.*x*(1.-x)*(3.*x-1.)
        else:
            return 0.5*x*(3.*x-1.)*(3.*x-2.)        
    def df(self,i,x):
        if (i == 0):
            return 0.5*(-27.*x*x+36.*x-11.)
        elif (i == 1):
            return -9./2.*(-9.*x*x+10.*x-2.)
        elif (i == 2):
            return 9./2.*(-9.*x*x+8.*x-1.)
        else:
            return 0.5*(27*x*x-18.*x+2.)
    # Fill Gradient Jump at interface
    # Continuous Interior Penalty
    def fill_Gr_jump(self, theta):
        em = cma.exp(-ic*theta)
        ep = cma.exp(ic*theta)         
        emm = cma.exp(-ic*2.*theta)
        epp = cma.exp(ic*2.*theta) 
        ct = cos(theta)
        ctt = cos(2.*theta)
        GJ = np.array([[-44.*ct+2.*ctt+123.,         -4.5*emm+58.5*em-103.5+9.*ep,  9.*emm-103.5*em+58.5-4.5*ep], \
                       [9.*em-103.5+58.5*ep-4.5*epp, -40.5*em+101.25-40.5*ep,       81.*em-81.+20.25*ep    ], \
                       [-4.5*em+58.5-103.5*ep+9.*epp, 20.25*em-81.+81.*ep,          -40.5*em+101.25-40.5*ep  ]]) 
        return GJ  
    


class Dispersion_lagrange_P4(Dispersion_lagrange):   
    # basis functions
    def f(self,i,x):
        if (i == 0):
            return -1./6.*(1.-x)*(4.*x-1)*(4.*x-2.)*(4.*x-3)
        elif (i == 1):
            return 8./3.*x*(1.-x)*(4.*x-2.)*(4.*x-3.)
        elif (i == 2):
            return -4.*x*(1.-x)*(4.*x-1)*(4.*x-3.)
        elif (i == 3):
            return 8./3.*x*(1.-x)*(4.*x-1.)*(4.*x-2.)        
        else:
            return 1./6.*x*(4.*x-1)*(4.*x-2.)*(4.*x-3)       


# =============================================================================
# Condensed Elements
# =============================================================================
class Dispersion_cubature(Dispersion_1D):
    pass
    
class Dispersion_cubature_P1(Dispersion_cubature):
    # basis functions
    def f(self,i,x):
        if (i == 0):
            return (1.-x)
        elif (i == 1):
            return x 
    def df(self,i,x):
        if (i == 0):
            return -1.
        elif (i == 1):
            return 1. 
    # Fill Gradient Jump at interface
    # Continuous Interior Penalty
    def fill_Gr_jump(self, theta):
        GJ = np.array([[(2.*cos(2.*theta)+6.-8.*cos(theta))]])
        return GJ
        
class Dispersion_cubature_P2(Dispersion_cubature):
    # basis functions
    def f(self,i,x):
        if (i == 0):
            return (1.-x)*(1.-2.*x)
        elif (i == 1):
            return 4.*x*(1.-x)
        elif (i == 2):
            return x*(2.*x-1.)     
    def df(self,i,x):
        if (i == 0):
            return 4.*x-3.
        elif (i == 1):
            return -8.*x+4.
        elif (i == 2):
            return 4.*x-1.
    # Fill Gradient Jump at interface
    # Continuous Interior Penalty
    def fill_Gr_jump(self, theta):
        em = cma.exp(-ic*theta)
        ep = cma.exp(ic*theta)         
        emm = cma.exp(-ic*2.*theta)
        epp = cma.exp(ic*2.*theta)         
        GJ = np.array([[24.*cos(theta)+38.+2.*cos(2.*theta), -28.-8.*cos(theta)-4.*emm-24.*em], \
                [-28.-24.*ep-8.*cos(theta)-4.*epp,          32.+32*cos(theta)]])
        return GJ
        
class Dispersion_cubature_P3(Dispersion_cubature):
    def __init__(self, el, bf, stab, Er):
        self.element_P = el
        self.basis_function = bf
        self.stab_method = stab            
        self.alp = 0.5 - 0.1*sqrt(5) 
        self.quad = Quadrature_droite(self.element_P, dof = True, bf = 'cubature')
        self.quad_DOF = Quadrature_droite(self.element_P, dof = True, bf = self.basis_function)
        self.Err = Er
        self.Err_msg = 'Phase : w'       

    # basis functions
    def f(self,i,x):
        s5 = 5.*sqrt(5.)
        if (i == 0):
            return 5.*(1.-x)*(x-self.alp)*(x-(1.-self.alp))
        elif (i == 1):
            return -s5*x*(1.-x)*(x-(1.-self.alp))
        elif (i == 2):
            return s5*x*(1.-x)*(x-self.alp)
        else:
            return 5.*x*(x-self.alp)*(x-(1.-self.alp))       
    def df(self,i,x):
        s5 = 5.*sqrt(5.)
        if (i == 0):
            return 5.*(-3.*x*x+4.*x-6./5.)
        elif (i == 1):
            return -s5*(-3.*x*x+2.*x*(2.-self.alp)-(1-self.alp))
        elif (i == 2):
            return s5*(-3.*x*x+2.*x*(1.+self.alp)-self.alp)
        else:
            return 5.*(3.*x*x-2.*x+1./5.)

    # Fill Gradient Jump at interface
    # Continuous Interior Penalty
    def fill_Gr_jump(self, theta):
        em = cma.exp(-ic*theta)
        ep = cma.exp(ic*theta)         
        emm = cma.exp(-ic*2.*theta)
        epp = cma.exp(ic*2.*theta) 
        p = 125.
        sr5 = 5.*sqrt(5)
        a0 = -self.alp
        a1 = (1.+11.*self.alp)
        a2 = -(12.-11.*self.alp)
        a3 = (1.-self.alp)
        b0 = -0.2
        b1 = (1.-2.*self.alp+2.*self.alp*self.alp)
        c0 = (0.8-self.alp)
        c1 = -0.4
        c2 = self.alp*self.alp
        GJ = np.array([[146.-24.*(em+ep)+(emm+epp),        sr5*(a0*emm + a1*em + a2 + a3*ep),   sr5*(a3*emm + a2*em + a1 + a0*ep ) ], \
                      [sr5*(a3*em + a2 + a1*ep + a0*epp),  p*(b0*em + b1 + b0*ep),   p*(c0*em + c1 + c2*ep)], \
                      [sr5*(a0*em + a1 + a2*ep + a3*epp),  p*(c2*em + c1 + c0*ep),   p*(b0*em + b1 + b0*ep)] ])
        return GJ


# =============================================================================
# Basic elements and Bernstein basis function
# =============================================================================
class Dispersion_bezier(Dispersion_1D):
    pass
    
class Dispersion_bezier_P1(Dispersion_bezier):
    # basis functions
    def f(self,i,x):
        if (i == 0):
            return (1.-x)
        elif (i == 1):
            return x 
    def df(self,i,x):
        if (i == 0):
            return -1.
        elif (i == 1):
            return 1.
    # Fill Gradient Jump at interface
    # Continuous Interior Penalty
    def fill_Gr_jump(self, theta):
        GJ = np.array([[(2.*cos(2.*theta)+6.-8.*cos(theta))]])
        return GJ
  
class Dispersion_bezier_P2(Dispersion_bezier):
    # basis functions
    def f(self,i,x):
        if (i == 0):
            return (1.-x)*(1.-x)
        elif (i == 1):
            return 2.*x*(1.-x)
        elif (i == 2):
            return x*x  
    def df(self,i,x):
        if (i == 0):
            return -2.*(1.-x)
        elif (i == 1):
            return 2.-4.*x
        elif (i == 2):
            return 2.*x
    # Fill Gradient Jump at interface
    # Continuous Interior Penalty
    def fill_Gr_jump(self, theta):
        em = cma.exp(-ic*theta)
        ep = cma.exp(ic*theta)                 
        GJ = np.array([[16., -8*(1.+em)], \
                [-8.*(1.+ep),  8.*(1.+cos(theta))]])
        return GJ    
    
class Dispersion_bezier_P3(Dispersion_bezier):   
    # basis functions
    def f(self,i,x):
        if (i == 0):
            return (1.-x)*(1.-x)*(1.-x)
        elif (i == 1):
            return 3.*(1.-x)*(1.-x)*x
        elif (i == 2):
            return 3.*x*x*(1.-x)
        else:
            return x*x*x        
    def df(self,i,x):
        if (i == 0):
            return -3.*(1.-x)*(1.-x)
        elif (i == 1):
            return -6.*(1.-x)*x + 3.*(1.-x)*(1.-x)
        elif (i == 2):
            return 6.*x*(1.-x) - 3.*x*x
        else:
            return 3.*x*x
    # Fill Gradient Jump at interface
    # Continuous Interior Penalty
    def fill_Gr_jump(self, theta):
        em = cma.exp(-ic*theta)
        ep = cma.exp(ic*theta)         
        GJ = np.array([[36., -18.,  -18.*em], \
                       [-18., 9.,    9.*em], \
                       [-18.*ep, 9.*ep, 9.]]) 
        return GJ      
