#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Feb  3 16:34:20 2020

@author: simichel
"""

import numpy as np

# =============================================================================
# Read file with parameters : penalty coef and cfl
# =============================================================================
def read_fileparam(fname):
    best_coef = []
    # read the bests coefficient file
    with open(fname,"r") as file:
        nb = 0
        for line in file:                                
            best_coef.append(line.split())
            best_coef[nb][4] = float(best_coef[nb][4])
            best_coef[nb][5] = float(best_coef[nb][5])
            nb += 1 
    return best_coef
        
    
# =============================================================================
# find CFL and tau
# =============================================================================
def find_tau_cfl(bf, st, ts, el):
    # parameter is a tab with :
    # basis function
    # stabilisation method
    # time scheme
    # element
    # time order
    #
    tab = read_fileparam("src/best_coef_tau_cfl-RES.txt")
    
    if (bf == 'bernstein'):
        bf2 = 'bezier'
    elif (bf == 'cubature'):
        bf2 = 'cohen'
    else:
        bf2 = bf
    if (st == 'No stab'):
        st = 'any'
    # time order
    to = el+1
    # ----------------------- main loop to find the coef ------------------- #
    nb = len(tab)
    idb = 0
    # find the identification number in the tab
    while (tab[idb][0] != bf2 or \
           tab[idb][1] != st or \
           tab[idb][2] != (ts+str(to)) or \
           tab[idb][3] != ('P'+str(el)) and idb < nb + 1 ):
        idb += 1
    # write the god cfl a penalty coefficient
    if (idb != nb):
        (tau,cfl) = (tab[idb][4], tab[idb][5])  
    elif (idb == nb):
        msg = 'best coef. are missing for ' + tab[idb][1] + ' ' + tab[idb][1] \
            + ' ' + tab[idb][2] + ' ' + tab[idb][3]
        return msg       
    #
    # Special cases, when there is no stable area
    Stable = True
    # Unstable scheme from the dispersion analysis
    if (st == 'any' and el == 1):
        Stable = False    
    elif ((bf2 == 'lagrange' or bf2 == 'bezier') \
          and ts == 'DeC' and st == 'any'):
        Stable = False        
    elif (bf2 == 'lagrange' and ts == 'DeC' and \
        (st == 'LPS' or st == 'CIP') and el == 3):
        Stable = False
    elif (st == 'SUPG' and bf2 == 'bezier' and ts == 'DeC'):
        Stable = False
    #
    return (tau,cfl,Stable)


