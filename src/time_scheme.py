#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 20 17:26:22 2021

@author: simichel
"""
import numpy as np # array

""" ---------------------- Time scheme classe ------------------------- """ 
# =============================================================================
# Runge-Kutta 
# =============================================================================
# Reference : Shu & Osher
# Efficient Implementation of essentially Non-oscillatory Shock-Capturing scheme
class time_scheme_RK():
    #
    # Initialize Time Scheme variables
    # Runge Kutta 
    # for RK and SSPRK scheme, we can write :
    # U^{n+1} = U^{n} + sum_{k=1}^{step} Gamma_k (-1*dt)^k F^{(k)}(U^{n}) 
    #
    def __init__(self, order):
        self.time_scheme = 'RK'
        # System parameters
        self.order = order
        if (self.order == 1):
            self.step = 1
            # Dispersion parameter
            self.Dispersion_G = [1.]
        elif (self.order == 2):
            # equivalent to the SSPRK(2,2)
            self.step = 2   
            # Dispersion parameter
            self.Dispersion_G = [1., 1./2.]            
        elif (self.order == 3):
            self.step = 3
            # Dispersion parameter
            self.Dispersion_G = [1., 1./2., 1./6.]
        elif (self.order == 4):
            self.step = 4
            self.Dispersion_G = [1., 1./2., 1./6., 1./24.]        
    # Destructor
    def __del__(self):
        del self.Dispersion_G

# =============================================================================
# Strong Stability preserving Runge-Kutta
# =============================================================================
class time_scheme_SSPRK():
    #
    # Initialize Time Scheme variables
    # Strong Stability Preserving Runge Kutta
    # for RK and SSPRK scheme, we can write :
    # U^{n+1} = U^{n} + sum_{k=1}^{step} Gamma_k (-1*dt)^k F^{(k)}(U^{n}) 
    #
    def __init__(self, order):
        self.time_scheme = 'SSPRK'
        # System parameters
        self.order = order
        # scheme parameters
        if (self.order == 1):
            self.step = 1
            # Dispersion parameter
            self.Dispersion_G = [1.]  
        elif (self.order == 2): 
            # SSPRK(3,2) Spitteri & Ruuth 2002
            self.step = 3
            # Dispersion parameter
            self.Dispersion_G = [1., 1./2., 1./12.]              
        elif (self.order == 3):
            # SSPRK(4,3) - Spitteri & Ruuth 2002
            self.step = 4      
            # Dispersion parameter
            self.Dispersion_G = [1., 1./2., 1./6., 1./48.]                        
        elif (self.order == 4):
            self.step = 5
            # SSPRK(5,4) - Ruuth 2006
            # Global optimization of explicit strong-stability-preserving Runge-Kutta methods
            # Dispersion parameter
            self.Dispersion_G = [1., 1./2., 1./6., 1./24., 0.004477718303076007]      
    # Destructor
    def __del__(self):
        del self.Dispersion_G

# =============================================================================
# Deferred Correction
# =============================================================================
class time_scheme_DeC():
    #
    # Initialize Time Scheme variables
    # DeC scheme
    #
    def __init__(self, order):
        self.time_scheme = 'DeC'
        # System parameters
        self.order = order
        # Quadrature form - time integration
        # theta[0] = [int_DT0 phi_j     for j = 0,M]
        # theta[1] = [int_DT1 phi_j     for j = 0,M]
        # etc
        # DT0
        # -- DT1 --
        # ----- DT2 ----
        # t0 -- t1 -- t2 -- tM
        # T(n) ------------ T(n+1)
        if (self.order == 1):
            self.Dt = [1.]
            self.theta = [[1.]]
        elif (self.order == 2):
            self.Dt = [0.,1.]
            self.theta = [[0.,0.], [0.5,0.5]]
        elif (self.order == 3):
            self.Dt = [0.,0.5,1.]
            self.theta = [[0.,0.,0.], [5./12.,8./12,-1./12.], \
                          [2./12.,8./12.,2./12.]]
        elif (self.order == 4):
            # uniform discretization 
            alp = 1./3.
            self.Dt = [0., alp, 1.-alp, 1.]
            self.theta = [[0.,0.,0.,0.], [27./72., 57./72., -15./72., 3./72.], \
                          [1.5/9., 6./9., 1.5/9., 0.], \
                          [1./8., 3./8., 3./8., 1./8.]]            
        elif (self.order == 5):
            # uniform discretization 
            alp = 1./4.
            self.Dt = [0., alp, 0.5, 1.-alp, 1.]
            self.theta = [[0, 0, 0, 0, 0],\
                          [32128.0/ 92160., 82688.0/ 92160., -33792.0/ 92160., 13568.0/ 92160., -2432.0/ 92160.] , \
                          [14848.0/ 92160., 63488.0/ 92160., 12288.0/ 92160., 2048.0/ 92160., -512.0/ 92160.] , \
                          [10368.0/ 92160., 39168.0/ 92160., 27648.0/ 92160., 16128.0/ 92160., -1152.0/ 92160.] , \
                          [7168.0/ 92160., 32768.0/ 92160., 12288.0/ 92160., 32768.0/ 92160., 7168.0/ 92160.]  ]           
    
    # for the DeC scheme and any elements, we can write :
    # U^{n+1} = G U^{n}
    def fourier_matrix1d(self, A, B, dt):
        # number of modes
        nm = A.shape[0]
        I = np.eye(nm)
        if (self.order == 1):
            G = I - dt*A 
        elif (self.order == 2):
            G = I - 2.*dt*A + dt*B.dot(A) + 0.5*dt*dt*A.dot(A)
        elif (self.order == 3):
            A2 = np.linalg.matrix_power(A,2)
            A3 = np.linalg.matrix_power(A,3)
            B2 = np.linalg.matrix_power(B,2)
            G = I - 3*dt*A + 3*dt*B.dot(A) - dt*B2.dot(A) \
                + 3./2.*dt*dt*A2 - 0.5*dt*dt*B.dot(A2) - 0.5*dt*dt*(A.dot(B)).dot(A) \
                - 1./6.*pow(dt,3)*A3
        elif (self.order == 4):
            A2 = np.linalg.matrix_power(A,2)
            A3 = np.linalg.matrix_power(A,3)
            A4 = np.linalg.matrix_power(A,4)
            B2 = np.linalg.matrix_power(B,2)
            B3 = np.linalg.matrix_power(B,3)
            BA = B.dot(A)
            AB = A.dot(B)
            G = I + dt*(-4.*A + 6*B.dot(A) - 4*B2.dot(A) + B3.dot(A)) \
                +  dt*dt*(3.*A2 - 2.*B.dot(A2) - 2.*A.dot(BA) + \
                          0.5*B2.dot(A2) + 0.5*BA.dot(BA) + 0.5*AB.dot(BA)) \
                +  pow(dt,3)*(-2./3.*A3 + 1./6.*B.dot(A3) + \
                       1./6.*AB.dot(A2) + 1./6.*A2.dot(BA)) \
                + 1./24.*pow(dt,4)*A4
        return G
    # Destructor
    def __del__(self):
        del self.theta
        del self.Dt

