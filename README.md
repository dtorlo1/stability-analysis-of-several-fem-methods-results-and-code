# Stability analysis of several FEM methods. Results and code

This is the public repository refering to the article [arXiv prepint](https://arxiv.org/abs/2103.16158) where we show the all the plots resulting from the stability analysis and the code used to produce those pictures, included the ones not available in the paper.

Moreover, it is available a **python app** to play with the parameters presented in the paper.

Dispersion/diffusion analysis app 
=========
Cloning the repository and running the app *main_dispersion.py* with python3 (tested with Python 3.8.0)
```
>> python main_dispersion.py
```
one can test the stability analysis we performed.
![App](/src/appScreenshot.png)

On the left and bottom one can choose between:
1. **elements** among basic (lagrange), cubature (cohen) and Bernstein (Bezier) 
1. **degrees** of the polynomials between 1 and 3, also contemporary
1. **time integration scheme** between RK, SSPRK and DeC
1. **stabilization technique** among SUPG, LPS and CIP or no stabilization
1. **CFL** condition
1. **penalty coefficient** for the stabilization technique
1. **Principal/all modes** can be switch on and off

All the details about the definitions of the stabilizations, the time integration schemes, the polynomials for different orders can be found in the [arXiv prepint](https://arxiv.org/abs/2103.16158)

Automatically, the CFL and τ stabilization coefficients are set according to the optimization technique performed in the work [arXiv prepint](https://arxiv.org/abs/2103.16158) corresponding to the minimization of η_u.

One can play with the different parameters and see what happens to the dispersion and damping coefficients.


Extra figures
========
It was impossible to include all the figures on the dispersion analysis and the simulations we run in [arXiv prepint](https://arxiv.org/abs/2103.16158). 

We run for the different elements, stabilizations, time integration methods, or with the parameters found with different optimization strategies.
In the file names, we use the code :
* `ELEMENT` to denote the different elements between `lagrange` (basic), `cohen` (cubature), `bezier` (Bernstein)
* `STABILIZATION` to denote the stabilization technique between `LPS`, `CIP`, `SUPG`, `any` (no stabilization)
* `TIME` for time discretization among `RK`, `SSPRK` and `DeC`
* `OPTIMIZATION` for the optimization technique used, where `best` optimizes only the CFL, `RES` optimizes the error η_u and `REW` optimizes η_ω

When not specified, the computations are done with the values found with the optimization of η_u. If there is no stable scheme in the (CFL, τ) the plots are not performed.

They can be found in this repository in folder [images](/images) organized as follows.

1. Stability analysis plots for linear advection problem in [images/StabilityAnalysis](/images/StabilityAnalysis)
    1. Spatial discretization analysis with optimal parameters found [SpatiallyDiscrete](/images/StabilityAnalysis/SpatiallyDiscrete). Files are denoted by `disc_space_ELEMENT_STABILIZATION_phase_OPTIMIZATION.pdf`
    1. Fully discretization analysis [FullyDiscrete](/images/StabilityAnalysis/FullyDiscrete). 
        1. The comparison of the result of the optimization techniques on the plane (CFL, τ) are in [stabilizationCFLplane](/images/StabilityAnalysis/FullyDiscrete/stabilizationCFLplane). Files are denoted as `all_param_disp_cfl_tau_ELEMENT_TIME_STABILIZATION.pdf`
        1. The dispersion and damping coefficients with respect to the phase for the found parameters are in [dispersionSelectedParameter](/images/StabilityAnalysis/FullyDiscrete/dispersionSelectedParameters). Files are denoted by `disp_full_rescale_OPTIMIZATION_ELEMENT_TIME_STABILIZATION_phase.pdf`
1. Simulation error results are in [Simulations](/images/Simulations).
    1. Linear advection tests are in [linearAdvection](/images/Simulations/linearAdvection)
        1. The convergence plots **number of degrees of freedom** vs **error** are in [convergence](/images/Simulations/linearAdvection/convergence) and files comparing the stabilization techniques are denoted by `stab_comparison_1D_LinearAdvection_TIME_ELEMENT_RES.pdf`
        1. The convergence plots **computational time** vs **error** are in [timeErrorPlot](/images/Simulations/linearAdvection/timeErrorPlot) and files comparing the stabilization techniques are denoted by `timeError_LinearAdvection_TIME_ELEMENT.pdf`
    1. Burgers' tests are in [Burgers](/images/Simulations/Burgers)
        1. The convergence plots **number of degrees of freedom** vs **error** are in [convergence](/images/Simulations/Burgers/convergence) and files comparing the stabilization techniques are denoted by `stab_comparison_1D_Burger_TIME_ELEMENT_RES.pdf`
        1. The convergence plots **computational time** vs **error** are in [timeErrorPlot](/images/Simulations/Burgers/timeErrorPlot) and files comparing the stabilization techniques are denoted by `timeError_Burger_TIME_ELEMENT.pdf`
    1. Shallow water tests are in [shallowWater](/images/Simulations/shallowWater)
        1. The convergence plots **number of degrees of freedom** vs **error** are in [convergence](/images/Simulations/shallowWater/convergence) and files comparing the stabilization techniques are denoted by `stab_comparison_1D_nlsw_TIME_ELEMENT_RES.pdf`
        1. The convergence plots **computational time** vs **error** are in [timeErrorPlot](/images/Simulations/shallowWater/timeErrorPlot) and files comparing the stabilization techniques are denoted by `timeError_nlsw_TIME_ELEMENT.pdf`

