#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 20 17:29:25 2021

@author: simichel
"""

import sys
# for the app interface
from PyQt5.QtCore import Qt
from PyQt5.QtGui import QIcon
from PyQt5.QtWidgets import (QApplication, QCheckBox, QDesktopWidget, \
        QDoubleSpinBox, QGridLayout, QGroupBox, QLabel, QMainWindow, \
        QPushButton, QRadioButton, QVBoxLayout, QWidget)
# for the plot
import pyqtgraph as pg
# my library
sys.path.insert(0, 'src/')
from full_discete_analysis import fourier_analysis, Scheme_param
from mesh_param import find_tau_cfl

#
color = ['r','b',	(34,200,34)]
#
class Dispersion_plot(QMainWindow):
    def __init__(self, parent=None):
        super(Dispersion_plot, self).__init__(parent)
        self.setWindowTitle("Disersion analysis for Linear advection equation")# <math> d<sub>t</sub> u + d<sub>x</sub> u = 0 </math>")
        self.originalPalette = QApplication.palette()
        # STYLE
#        self.setStyle('Fusion')
        # MENU BAR
        self.menuBar = self.menuBar()
        self.menuBar.addAction("File")
        self.menuBar.addAction("View")
        self.resize(1400, 600)
        self.center()
        # INITIALIZE THE SCHEME
        self.scheme = Scheme_param()
        # DIFFERENT BOXES
        self.createElementGroupBox()
        self.createDegreeGroupBox()
        self.createTimeSchemeGroupBox()
        self.createStabilizationGroupBox()
        self.createParametersGroupBox()
        self.creatGraphicOptionsBox()
        self.creatGraphicGroupBox()
        # update best parameters
        self.update_best_param()
        # LOCATION OF BOXES 
        # (row, col, length row, length col)
        mainLayout = QGridLayout()
        # left part
        mainLayout.addWidget(self.ElementGroupBox, 1, 0, 1, 2)
        mainLayout.addWidget(self.DegreeGroupBox, 2, 0, 1, 2)
        mainLayout.addWidget(self.TimeSchemeGroupBox, 3, 0, 1, 1)
        mainLayout.addWidget(self.StabilizationGroupBox, 3, 1, 1, 1)
        mainLayout.addWidget(self.ParametersGroupBox, 4, 0, 1, 4)
        # right part
        mainLayout.addWidget(self.GraphicGroupBoxDispersion, 1 , 2, 3, 3)
        mainLayout.addWidget(self.GraphicGroupBoxDamping, 1 , 5, 3, 3)
        mainLayout.addWidget(self.GraphicOptionsBox, 4 , 4, 1, 4)
        #
        wid = QWidget()
        wid.setLayout(mainLayout)
        self.setCentralWidget(wid)
    #
    def center(self):
        screen = QDesktopWidget().screenGeometry()
        size = self.geometry()
        self.move((screen.width() - size.width()) / 2, (screen.height() - size.height()) / 2)
    #
    # Choose among different type of elements
    #
    def createElementGroupBox(self):
        self.ElementGroupBox = QGroupBox("Elements")
        #
        self.EradioButtonCub = QRadioButton("Cubature with lagrange basis function")
        self.EradioButtonCub.clicked.connect(self.update_best_param)
        self.EradioButtonLag = QRadioButton("Basic with Lagrange basis function")
        self.EradioButtonLag.clicked.connect(self.update_best_param)
        self.EradioButtonBern = QRadioButton("Basic with Bernstein basis function")
        self.EradioButtonBern.clicked.connect(self.update_best_param)
        self.EradioButtonLag.setChecked(True)
        #
        checkBox = QCheckBox("Tri-state check box")
        checkBox.setTristate(True)
        checkBox.setCheckState(Qt.PartiallyChecked)
        layout = QVBoxLayout()
        layout.addWidget(self.EradioButtonCub)
        layout.addWidget(self.EradioButtonLag)
        layout.addWidget(self.EradioButtonBern)
        self.ElementGroupBox.setLayout(layout)    
    #
    # Choose to plot or not the dispersion curve
    # 
    def createDegreeGroupBox(self):
        self.DegreeGroupBox = QGroupBox("Degree")
        # P1 Element
        self.P1PushButton = QPushButton("P1")
        self.P1PushButton.setCheckable(True)
        self.P1PushButton.clicked.connect(self.update_Graphic)
        # P2 Element
        self.P2PushButton = QPushButton("P2")
        self.P2PushButton.setCheckable(True)
        self.P2PushButton.setChecked(False)
        self.P2PushButton.clicked.connect(self.update_Graphic)
        # P3 Element
        self.P3PushButton = QPushButton("P3")
        self.P3PushButton.setCheckable(True)
        self.P3PushButton.setChecked(False)
        self.P3PushButton.clicked.connect(self.update_Graphic)
        # 
        layout = QVBoxLayout()
        self.DegreePushButton = [self.P1PushButton, self.P2PushButton, self.P3PushButton]
        layout.addWidget(self.P1PushButton)
        layout.addWidget(self.P2PushButton)
        layout.addWidget(self.P3PushButton)
        self.DegreeGroupBox.setLayout(layout)
    #
    # Choose among different time schemes
    #
    def createTimeSchemeGroupBox(self):
        self.TimeSchemeGroupBox = QGroupBox("Time Scheme")
        #
        self.TS_radioButtonRK = QRadioButton("RK")
        self.TS_radioButtonRK.clicked.connect(self.update_best_param)
        self.TS_radioButtonSSPRK = QRadioButton("SSPRK")
        self.TS_radioButtonSSPRK.clicked.connect(self.update_best_param)
        self.TS_radioButtonDeC = QRadioButton("DeC")
        self.TS_radioButtonDeC.clicked.connect(self.update_best_param)
        self.TS_radioButtonRK.setChecked(True)
        #
        checkBox = QCheckBox("Tri-state check box")
        checkBox.setTristate(True)
        checkBox.setCheckState(Qt.PartiallyChecked)
        # 
        layout = QVBoxLayout()
        layout.addWidget(self.TS_radioButtonRK)
        layout.addWidget(self.TS_radioButtonSSPRK)
        layout.addWidget(self.TS_radioButtonDeC)
        self.TimeSchemeGroupBox.setLayout(layout)  
    #
    # Choose among different time schemes
    #
    def createStabilizationGroupBox(self):
        self.StabilizationGroupBox = QGroupBox("Stabilization")
        #
        self.SradioButtonNo = QRadioButton("No stabilization")
        self.SradioButtonNo.clicked.connect(self.update_best_param)
        self.SradioButtonSUPG = QRadioButton("SUPG")
        self.SradioButtonSUPG.clicked.connect(self.update_best_param)
        self.SradioButtonLPS = QRadioButton("LPS")
        self.SradioButtonLPS.clicked.connect(self.update_best_param)
        self.SradioButtonCIP = QRadioButton("CIP")
        self.SradioButtonCIP.clicked.connect(self.update_best_param)
        self.SradioButtonNo.setChecked(True)
        #
        checkBox = QCheckBox("Tri-state check box")
        checkBox.setTristate(True)
        checkBox.setCheckState(Qt.PartiallyChecked)
        # 
        layout = QVBoxLayout()
        layout.addWidget(self.SradioButtonNo)
        layout.addWidget(self.SradioButtonSUPG)
        layout.addWidget(self.SradioButtonLPS)
        layout.addWidget(self.SradioButtonCIP)
        self.StabilizationGroupBox.setLayout(layout)   
    #
    def setCFL(self, cflBox):
        cflBox.setRange(0.01,1.5)
        cflBox.setSingleStep(1e-3)
        cflBox.setValue(0.1)
        cflBox.setDecimals(3)
        cflBox.valueChanged.connect(self.update_Graphic)
    def setTAU(self, tauBox):
        tauBox.setRange(1e-5,1.)
        tauBox.setSingleStep(1e-5)
        tauBox.setValue(0.01)
        tauBox.setDecimals(5)
        tauBox.valueChanged.connect(self.update_Graphic)
    def update_best_param(self):
        #
        # Parameters
        #
        # Element
        if (self.EradioButtonCub.isChecked() == True):
            self.scheme.basis_function = 'cubature'
        elif (self.EradioButtonLag.isChecked() == True):
            self.scheme.basis_function = 'lagrange'
        elif (self.EradioButtonBern.isChecked() == True):
            self.scheme.basis_function = 'bernstein'       
        # Time scheme
        if (self.TS_radioButtonRK.isChecked() == True):
            self.scheme.time_scheme = 'RK'
        elif (self.TS_radioButtonSSPRK.isChecked() == True):
            self.scheme.time_scheme = 'SSPRK'
        elif (self.TS_radioButtonDeC.isChecked() == True):
            self.scheme.time_scheme = 'DeC'
        # Stabilization
        if (self.SradioButtonNo.isChecked() == True):
            self.scheme.stabilization = 'No stab'
        elif (self.SradioButtonSUPG.isChecked() == True):
            self.scheme.stabilization = 'SUPG'
        elif (self.SradioButtonLPS.isChecked() == True):
            self.scheme.stabilization = 'LPS'            
        elif (self.SradioButtonCIP.isChecked() == True):
            self.scheme.stabilization = 'CIP' 
        
        # INITIALISE WITH BEST PARAMETERS
        # P1 ELEMENT
        (tau,cfl,Stable) = find_tau_cfl( self.scheme.basis_function, \
            self.scheme.stabilization, self.scheme.time_scheme, 1)
        if (Stable == False):
            self.P1PushButton.setChecked(True)
            self.spinBoxtauP1.setValue(0.01)
            self.spinBoxcflP1.setValue(0.1)
        else:
            self.P1PushButton.setChecked(False)   
            self.spinBoxtauP1.setValue(tau)
            self.spinBoxcflP1.setValue(cfl)            
        # P2 ELEMENT
        (tau,cfl,Stable) = find_tau_cfl( self.scheme.basis_function, \
            self.scheme.stabilization, self.scheme.time_scheme, 2)
        if (Stable == False):
            self.P2PushButton.setChecked(True)        
            self.spinBoxtauP2.setValue(0.01)
            self.spinBoxcflP2.setValue(0.1)   
        else:
            self.P2PushButton.setChecked(False)   
            self.spinBoxtauP2.setValue(tau)
            self.spinBoxcflP2.setValue(cfl) 
        # P3 ELEMENT
        (tau,cfl,Stable) = find_tau_cfl( self.scheme.basis_function, \
            self.scheme.stabilization, self.scheme.time_scheme, 3)
        if (Stable == False):
            self.P3PushButton.setChecked(True)   
            self.spinBoxtauP3.setValue(0.01)
            self.spinBoxcflP3.setValue(0.1)            
        else:
            self.P3PushButton.setChecked(False)   
            self.spinBoxtauP3.setValue(tau)
            self.spinBoxcflP3.setValue(cfl)     
        self.update_Graphic()
    #
    # Choose Parameters of the scheme
    #
    def createParametersGroupBox(self):
        self.ParametersGroupBox = QGroupBox("Parameters")
        # CFL
        self.spinBoxcflP1 = QDoubleSpinBox(self.ParametersGroupBox)
        self.setCFL(self.spinBoxcflP1)
        self.spinBoxcflP2 = QDoubleSpinBox(self.ParametersGroupBox)
        self.setCFL(self.spinBoxcflP2)        
        self.spinBoxcflP3 = QDoubleSpinBox(self.ParametersGroupBox)
        self.setCFL(self.spinBoxcflP3)        
        # Penalty coefficient
        self.spinBoxtauP1 = QDoubleSpinBox(self.ParametersGroupBox)
        self.setTAU(self.spinBoxtauP1)
        self.spinBoxtauP2 = QDoubleSpinBox(self.ParametersGroupBox)
        self.setTAU(self.spinBoxtauP2)       
        self.spinBoxtauP3 = QDoubleSpinBox(self.ParametersGroupBox)
        self.setTAU(self.spinBoxtauP3)        
        #
        # print label and value
        layout = QGridLayout()
        # row 0
        P1Label = QLabel("P1")
        layout.addWidget(P1Label, 0, 2)       
        P2Label = QLabel("P2")
        layout.addWidget(P2Label, 0, 3)    
        P3Label = QLabel("P3")
        layout.addWidget(P3Label, 0, 4)            
        # row 1
        cflLabel = QLabel("CFL")
        layout.addWidget(cflLabel, 1, 0, 1, 2)
        layout.addWidget(self.spinBoxcflP1, 1, 2)
        layout.addWidget(self.spinBoxcflP2, 1, 3)
        layout.addWidget(self.spinBoxcflP3, 1, 4)
        # row 2
        tauLabel = QLabel("Penalty coefficient \u03C4")
        layout.addWidget(tauLabel, 2, 0, 1, 2)
        layout.addWidget(self.spinBoxtauP1, 2, 2)
        layout.addWidget(self.spinBoxtauP2, 2, 3)
        layout.addWidget(self.spinBoxtauP3, 2, 4)
        #
        self.ParametersGroupBox.setLayout(layout)
    #
    # Generic function for the plot 
    #
    def plot_dispersion(self, el_tab, dx, THETA, PRINCIPAL_CK, PRINCIPAL_DAMP, ALL_CK, ALL_DAMP):
        #
        # DISPERSION PLOT Group
        self.GraphicGroupBoxDispersion.setBackground('w')
        self.GraphicGroupBoxDispersion.setTitle("Disersion")
        # axis limit
        self.GraphicGroupBoxDispersion.setXRange(0, 3.15, padding=0)
        self.GraphicGroupBoxDispersion.setYRange(-0.1, 3.15, padding=0)
        # legend
        self.GraphicGroupBoxDispersion.addLegend()
        if (self.scheme.basis_function == 'cubature'):
            sP = '<math> P<sub>'
        else:
            sP = '<math> P<sub>' 
        for element in el_tab:
            if (self.principalPushButton.isChecked() == False):
                if (self.DegreePushButton[element-1].isChecked() == False):
                    lg = sP+str(element)+'</sub></math> - principal mode'
                    Pen = pg.mkPen(color=color[element-1], width=2, style=Qt.SolidLine)
                    self.GraphicGroupBoxDispersion.plot(THETA[element-1],PRINCIPAL_CK[element-1], pen = Pen, name = lg)
            if (self.allPushButton.isChecked() == False):
                if (self.DegreePushButton[element-1].isChecked() == False):
                    lg = sP+str(element)+'</sub></math> - all modes'
                    Pen = pg.mkPen(color=color[element-1], width=1, style=Qt.DashDotDotLine)
                    self.GraphicGroupBoxDispersion.plot(THETA[element-1],ALL_CK[element-1][0], pen = Pen, name = lg)                       
                    for l in range(1,element):
                        self.GraphicGroupBoxDispersion.plot(THETA[element-1],ALL_CK[element-1][l], pen = Pen)                          
        # exact solution
        wex = THETA[2][:] * 1./dx
        Pen = pg.mkPen(color='k', width=2, style=Qt.DashLine)
        self.GraphicGroupBoxDispersion.plot(THETA[2][:],wex, pen = Pen, name = '\u03C9 ex = a*k')
        # axis name
        styles = {'color':'r', 'font-size':'20px'}
        self.GraphicGroupBoxDispersion.setLabel('left','Phase : \u03C9', **styles)
        self.GraphicGroupBoxDispersion.setLabel('bottom','k*dx*deg = 2*pi / Nc', **styles)
        # grid
        self.GraphicGroupBoxDispersion.showGrid(x=True, y=True)
        #
        # DAMPING PLOT Group
        self.GraphicGroupBoxDamping.setBackground('w')
        self.GraphicGroupBoxDamping.setTitle("Damping")
        # axis limit
        self.GraphicGroupBoxDamping.setXRange(0., 3.15, padding=0)   
        self.GraphicGroupBoxDamping.setYRange(-4., 0.5, padding=0)
        # legend
        self.GraphicGroupBoxDamping.addLegend()
        if (self.scheme.basis_function == 'cubature'):
            sP = '<math> P<sub>'
        else:
            sP = '<math> P<sub>' 
        for element in el_tab:
            if (self.principalPushButton.isChecked() == False):
                if (self.DegreePushButton[element-1].isChecked() == False):
                    lg = sP+str(element)+'</sub></math> - principal eigenvalue'
                    Pen = pg.mkPen(color=color[element-1], width=2, style=Qt.SolidLine)
                    self.GraphicGroupBoxDamping.plot(THETA[element-1],PRINCIPAL_DAMP[element-1], pen = Pen, name = lg)
            if (self.allPushButton.isChecked() == False):
                if (self.DegreePushButton[element-1].isChecked() == False):
                    lg = sP+str(element)+'</sub></math> - all modes'
                    Pen = pg.mkPen(color=color[element-1], width=1, style=Qt.DashDotDotLine)
                    self.GraphicGroupBoxDamping.plot(THETA[element-1],ALL_DAMP[element-1][0], pen = Pen, name = lg)                       
                    for l in range(1,element):
                        self.GraphicGroupBoxDamping.plot(THETA[element-1],ALL_DAMP[element-1][l], pen = Pen)   
        # axis name
        styles = {'color':'r', 'font-size':'20px'}
        self.GraphicGroupBoxDamping.setLabel('left','Damping : \u03B5 = log (| \u03BB |) / dt', **styles)
        self.GraphicGroupBoxDamping.setLabel('bottom','k*dx*deg = 2*pi / Nc', **styles)
        # grid
        self.GraphicGroupBoxDamping.showGrid(x=True, y=True)                
    #
    # Plot the dispersion curve and the respective Damping
    #         
    def creatGraphicGroupBox(self):
        # PLOT GRAPH
        # Dispersion Group
        self.GraphicGroupBoxDispersion = pg.PlotWidget()
        self.GraphicGroupBoxDispersion.setBackground('w')
        # Damping Group
        self.GraphicGroupBoxDamping = pg.PlotWidget()
        self.GraphicGroupBoxDamping.setBackground('w')        
        #
        # Parameters
        #
        # Element
        if (self.EradioButtonCub.isChecked() == True):
            self.scheme.basis_function = 'cubature'
        elif (self.EradioButtonLag.isChecked() == True):
            self.scheme.basis_function = 'lagrange'
        elif (self.EradioButtonBern.isChecked() == True):
            self.scheme.basis_function = 'bernstein'       
        # Time scheme
        if (self.TS_radioButtonRK.isChecked() == True):
            self.scheme.time_scheme = 'RK'
        elif (self.TS_radioButtonSSPRK.isChecked() == True):
            self.scheme.time_scheme = 'SSPRK'
        elif (self.TS_radioButtonDeC.isChecked() == True):
            self.scheme.time_scheme = 'DeC'
        # Stabilization
        if (self.SradioButtonNo.isChecked() == True):
            self.scheme.stabilization = 'No stab'
        elif (self.SradioButtonSUPG.isChecked() == True):
            self.scheme.stabilization = 'SUPG'
        elif (self.SradioButtonLPS.isChecked() == True):
            self.scheme.stabilization = 'LPS'            
        elif (self.SradioButtonCIP.isChecked() == True):
            self.scheme.stabilization = 'CIP'            
        # CFL and Penalty coefficient
#        self.update_best_param()
        self.scheme.cfl = [self.spinBoxcflP1.value(), self.spinBoxcflP2.value(), self.spinBoxcflP3.value()]
        self.scheme.Penalty_coef = [self.spinBoxtauP1.value(), self.spinBoxtauP2.value(), self.spinBoxtauP3.value()]
        # 
        # Run the dispersion
        #
        (el_tab, dx, THETA, PRINCIPAL_CK, PRINCIPAL_DAMP, ALL_CK, ALL_DAMP) = \
            fourier_analysis(self.scheme.basis_function, self.scheme.time_scheme, \
            stabilization = self.scheme.stabilization, cfl_tab = self.scheme.cfl, \
            Penalty_coef_tab = self.scheme.Penalty_coef)
        # Plot data
        self.plot_dispersion(el_tab, dx, THETA, PRINCIPAL_CK, PRINCIPAL_DAMP, ALL_CK, ALL_DAMP)       
    #
    # Plot the dispersion curve and the respective Damping
    #             
    def update_Graphic(self):
        self.GraphicGroupBoxDispersion.clear()
        self.GraphicGroupBoxDamping.clear()
        #
        # Update parameters
        #
        # Element
        if (self.EradioButtonCub.isChecked() == True):
            self.scheme.basis_function = 'cubature'
        elif (self.EradioButtonLag.isChecked() == True):
            self.scheme.basis_function = 'lagrange'
        elif (self.EradioButtonBern.isChecked() == True):
            self.scheme.basis_function = 'bernstein'       
        # Time scheme
        if (self.TS_radioButtonRK.isChecked() == True):
            self.scheme.time_scheme = 'RK'
        elif (self.TS_radioButtonSSPRK.isChecked() == True):
            self.scheme.time_scheme = 'SSPRK'
        elif (self.TS_radioButtonDeC.isChecked() == True):
            self.scheme.time_scheme = 'DeC'
        # Stabilization
        if (self.SradioButtonNo.isChecked() == True):
            self.scheme.stabilization = 'No stab'
        elif (self.SradioButtonSUPG.isChecked() == True):
            self.scheme.stabilization = 'SUPG'
        elif (self.SradioButtonLPS.isChecked() == True):
            self.scheme.stabilization = 'LPS'            
        elif (self.SradioButtonCIP.isChecked() == True):
            self.scheme.stabilization = 'CIP'            
        # CFL and Penalty coefficient
        self.scheme.cfl = [self.spinBoxcflP1.value(), self.spinBoxcflP2.value(), self.spinBoxcflP3.value()]
        self.scheme.Penalty_coef = [self.spinBoxtauP1.value(), self.spinBoxtauP2.value(), self.spinBoxtauP3.value()]
        # 
        # Run the dispersion
        #
        (el_tab, dx, THETA, PRINCIPAL_CK, PRINCIPAL_DAMP, ALL_CK, ALL_DAMP) = \
            fourier_analysis(self.scheme.basis_function, self.scheme.time_scheme, \
            stabilization = self.scheme.stabilization, cfl_tab = self.scheme.cfl, \
            Penalty_coef_tab = self.scheme.Penalty_coef)
        # Plot data
        self.plot_dispersion(el_tab, dx, THETA, PRINCIPAL_CK, PRINCIPAL_DAMP, ALL_CK, ALL_DAMP)       

# =============================================================================
# 
# =============================================================================
    #
    # Options for the plot
    #  
    def creatGraphicOptionsBox(self):
        self.GraphicOptionsBox = QGroupBox("Plot options")
        # plot the principal mode
        self.principalPushButton = QPushButton("Plot the principal mode")
        self.principalPushButton.setCheckable(True)
        self.principalPushButton.clicked.connect(self.update_Graphic)
        # plot all modes / eigenvalues
        self.allPushButton = QPushButton("Plot all modes / eigenvalues")
        self.allPushButton.setCheckable(True)
        self.allPushButton.setChecked(False)
        self.allPushButton.clicked.connect(self.update_Graphic)
        # 
        layout = QGridLayout()
        layout.addWidget(self.principalPushButton, 0, 0)
        layout.addWidget(self.allPushButton, 0, 1)
        self.GraphicOptionsBox.setLayout(layout)

if __name__ == '__main__':
    # APPLICATION SYSTEM
    app = QApplication(sys.argv)
    app.setWindowIcon(QIcon('src/icon.png'))
    # MY APP
    software = Dispersion_plot()
    software.show()
    sys.exit(app.exec_()) 