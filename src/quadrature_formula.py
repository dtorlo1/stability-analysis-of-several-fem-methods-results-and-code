#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 20 17:28:15 2021

@author: simichel
"""


from math import sqrt
import numpy as np

def barycentric_coord_tri(a,b,c,x):
    K = ((b[1]-c[1])*(a[0]-c[0])+(c[0]-b[0])*(a[1]-c[1]))
    alpha = ((b[1]-c[1])*(x[0]-c[0])+(c[0]-b[0])*(x[1]-c[1])) / K
    beta = ((c[1]-a[1])*(x[0]-c[0])+(a[0]-c[0])*(x[1]-c[1])) / K
    gamma = 1 - alpha - beta
    p = np.array((alpha, beta, gamma))
    return p
        
class Quadrature_droite:
    # Initialisation of the quadrature formula
    # definition of quadrature points and corresponding weight 
    def __init__(self, d, dof = False, bf = None):
        m = 0.5
        a0 = [0.,0.]
        a1 = [1.,0.]
        a2 = [0.,1.]
        if (dof == True):
            if (d == 1):
                n = 2                
                # Weights
                self.w = [0 for i in range(n)] 
                # quadrature point
                self.qp = [[0 for i in range(3)] for j in range(n)] 
                self.qp = [[1.,0.,0.],[0.,1.,0.]]
                self.w = [0.5, 0.5]
            elif (d == 2): 
                n = 3                
                # Weights
                if (bf == 'cubature' or bf == 'lagrange'):
                    self.w = [1./6., 2./3., 1./6.]
                elif (bf == 'bezier' or bf == 'bernstein'):
                    self.w = [1./3., 1./3., 1./3.]
                # quadrature point
                self.qp = [[1.,0.,0.],[0.5,0.5,0.],[0.,1.,0.]]
            elif (d >= 3):
                n = 3       
                if (bf == 'cubature'):
                    alp = (-15.*sqrt(7.)-21.+sqrt(168.+174.*sqrt(7.))) / (2.*(-15*sqrt(7)-21))
                elif (bf == 'lagrange' or bf == 'bezier' or bf == 'bernstein'):
                    alp = 1./3.
                if (bf == 'cubature' or bf == 'lagrange'):
                    w0 = 1./ (12*alp*(1.-alp)) * (-1.+6.*alp*(1.-alp))
                elif (bf == 'bezier' or bf == 'bernstein'):
                    w0 = 1./4.
                w1 = 0.5 * (1.-2.*w0)
                # Weights
                self.w = [0 for i in range(n)] 
                # quadrature point
                self.qp = [[0 for i in range(3)] for j in range(n)] 
                self.qp = [[1.,0.,0.],[1-alp,alp,0.],[alp,1-alp,0],[0.,1.,0.]]
                self.w = [w0, w1, w1, w0]     
        else:
            if (d == 1):
                """ quadrature method - order 3 """
                n = 2
                # Weights
                self.w = [0 for i in range(n)] 
                # quadrature point
                self.qp = [[0 for i in range(3)] for j in range(n)] 
                b1 = m + 0.5*sqrt(3.)/3
                b2 = m - 0.5*sqrt(3.)/3
                # Quadrature points order 3
                self.qp[0] = barycentric_coord_tri(a0,a1,a2, [b1, 0.])
                self.qp[1] = barycentric_coord_tri(a0,a1,a2, [b2, 0.])
                # Weights
                self.w = [0.5, 0.5]  
            elif (d == 2): 
                # P = 0 for all quadrature points, except P_{id_K} (id_K) = 1
                #       and P_{id_K} (1/3,1/3,1/3) = - 1/9 or 4/9
                """ quadrature method - order 5 """
                n = 3
                # Weights
                self.w = [0 for i in range(n)] 
                # quadrature point
                self.qp = [[0 for i in range(3)] for j in range(n)] 
                
                b1 = m + 0.5*sqrt(3./5.)
                b2 = m - 0.5*sqrt(3./5.)
                # Quadrature points order 3
                self.qp[0] = barycentric_coord_tri(a0,a1,a2, [b1, 0.])
                self.qp[1] = barycentric_coord_tri(a0,a1,a2, [b2, 0.])
                self.qp[2] = barycentric_coord_tri(a0,a1,a2, [m, 0.])
                # Weights
                self.w = [5./18., 5./18., 8./18.]  
            elif (d >= 3):
                """ quadrature method - order 7 """
                n = 4
                # Weights
                self.w = [0 for i in range(n)] 
                # quadrature point
                self.qp = [[0 for i in range(3)] for j in range(n)] 
    
                b1 = m + 0.5*sqrt(1./35.*(15.+2*sqrt(30.)))
                b2 = m - 0.5*sqrt(1./35.*(15.+2*sqrt(30.)))
                b3 = m + 0.5*sqrt(1./35.*(15.-2*sqrt(30.)))
                b4 = m - 0.5*sqrt(1./35.*(15.-2*sqrt(30.))) 
                
                w1 = (1./4. - 1./12.*sqrt(5./6.))
                w2 = (1./4. + 1./12.*sqrt(5./6.))
                # Quadrature points order 3
                self.qp[0] = barycentric_coord_tri(a0,a1,a2, [b1, 0.])
                self.qp[1] = barycentric_coord_tri(a0,a1,a2, [b2, 0.])
                self.qp[2] = barycentric_coord_tri(a0,a1,a2, [b3, 0.])
                self.qp[3] = barycentric_coord_tri(a0,a1,a2, [b4, 0.]) 
                # Weights
                self.w = [w1, w1, w2, w2]  

    def __del__(self):
        del self.qp
        del self.w